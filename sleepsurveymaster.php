<?php
/**
 * Test2
 * Template Name: Sleep Survey - Baby Sleep Answers
 * Version: 0.1
 * Description: Sleep Survey PDF created from customer input
 * Author: Greg Spenla
 * Author URI: https://gravitypdf.com
 * Group: Baby Sleep Answers
 * License: GPLv2
 * Required PDF Version: 4.0
 * Tags: space, solar system, getting started
 */


/* Prevent direct access to the template (always good to include this) */
if ( ! class_exists( 'GFForms' ) ) {
  return;
}
/**
 * Load our template-specific settings
 */
$show_meta_data = ! empty( $settings['world_show_meta_data'] ) ? $settings['world_show_meta_data'] : 'No';
/**
 * Include your PHP variables in this section
 * Note that my variables contain the form id number in them
 */
$email_32 = $form_data['field'][32];
$fname_31_first   = $form_data['field'][31]['first'];
$lname_31_last   = $form_data['field'][31]['last'];
$babyname_3 = $form_data['field'][3];
$babybirthdate_5 = $form_data['field'][5];
$sleepgoal_11 = $form_data['field'][11];
$dob_5 = $form_data['field'][5]; /* change the ID if your date field has something different */
    $dob_iso_format = date( 'Y-m-d', strtotime( $dob_5 ) );
    $dob_datetime  = new DateTime( $dob_iso_format );
    $datediff = $dob_datetime->diff(new DateTime ());
    $age_5_months = $datediff->format('%m') + 12 * $datediff->format('%y');
    $age_5_years = $datediff->format('%y');
    $age_5_days = $datediff->format('%d');
$agemeasure_5= "years";
$Gender_15 = $form_data['field'][15];
$sleepgoal_16 = $form_data['field'][16];
$sleepcrutches_25 = $form_data['field'][25];
$numberofnaps_24 = $form_data['field'][24];
$isnaptimeroutine_19 = $form_data['field'][19];
$iswinddown_22 = $form_data['field'][22];
$cosleeping_23 = $form_data['field'][23];
$crying_29 = $form_data['field'][29];
$otherkids_17 = $form_data['field'][17];
$scheduleconflicts_18 = $form_data['field'][18];
$joke_30 = $form_data['field'][30];
$sleepingthroughthenight_34 = $form_data['field'][34];

if ($Gender_15 === 'Boy') {
	$capHeOrShe_15 = 'He';
	$capHisOrHers_15 = 'His';
	$capHisOrHer_15 = 'His';
	$capHimOrHer_15 = 'Him';
	$capHimselfOrHerself = 'Himself';
	$lowHeOrShe_15 = 'he';
	$lowHisOrHers_15 = 'his';
	$lowHisOrHer_15 = 'his';
	$lowHimOrHer_15 = 'him';
	$lowHimselfOrHerself_15 = 'himself';
	} 
	
if ($Gender_15 === 'Girl') {
	$capHeOrShe_15 = 'Her';
	$capHisOrHers_15 = 'Hers';
	$capHisOrHer_15 = 'Her';
	$capHimOrHer_15 = 'Her';
	$capHimselfOrHerself_15 = 'Herself';
	$lowHeOrShe_15 = 'she';
	$lowHisOrHers_15 = 'hers';
	$lowHisOrHer_15 = 'her';
	$lowHimOrHer_15 = 'her';
	$lowHimselfOrHerself_15 = 'herself';
	}

if ( $age_5 === 0) {
	$age_5 = $dob_datetime->diff( new DateTime( 'today' ) )->m;
	if ($age_5 === 1) {
	  $agemeasure_5= "month";} else {
	  $agemeasure_5= "months";}
	}

if ( $age_5 === 0) {
	$age_5 = $dob_datetime->diff( new DateTime( 'today' ) )->d;
	$agemeasure_5 = "days";
	}
	
?>


<!-- Any PDF CSS styles can be placed in the style tag below -->
<style>
    /* Prevent direct access to the template (always good to include this)
        text-align: center;
        text-transform: uppercase;
        color: #a62828;
        border-bottom: 1px solid #999;
        }*/

    h0  {
        font-size: 25px;
        letter-spacing: 2px;
        word-spacing: 2px;
        color: #000000;
        font-weight: 700;
        text-decoration: none;
        font-style: italic;
        font-variant: small-caps;
        text-transform: capitalize;
        }
        
   h1  {
        font-size: 20px;
        letter-spacing: 2px;
        word-spacing: 2px;
        color: #636464;
        font-weight: 800;
        text-decoration: none;
        font-style: normal;
        font-variant: small-caps;
        text-transform: none;
        }

    h2  {
        font-family: Open Sans;
        font-size: 15px;
        letter-spacing: 2px;
        word-spacing: 2px;
        color: #3b6196;
        font-weight: 400;
        text-decoration: none;
        font-style: normal;
        font-variant: normal;
        text-transform: none;
        }

    h3  {
        font-family: Open Sans;
        font-size: 15px;
        letter-spacing: 2px;
        word-spacing: 2px;
        color: #636464;
        font-weight: 200;
        text-decoration: none;
        font-style: italic;
        font-variant: normal;
        text-transform: none;
        }

  <!--
   .tab { margin-left: 40px; }
   .tab2 { margin-left: 80px; }
   .tab3 { margin-left: 120px; }
  -->

  pagebreak {
      page-break-after: always;
  }
  
  table {
    width:82%;
    margin-left:9%;
    margin-righ:9%

    }
    table, th, td {
     border: 1px solid black;
     border-collapse: collapse;
    }
    th, td {
     padding: 5px;
     text-align: center;
    }
    table#t01 tr:nth-child(even) {
     background-color: #eee;
    }
    table#t01 tr:nth-child(odd) {
     background-color: #fff;
    }
    table#t01 th {
     background-color: #D5A6BC;
     color: black;
    }
  @page {
    margin-top: 2cm;
    margin-bottom: 2cm;
    margin-left: 2cm;
    margin-right: 2cm;
  }
  
/* This entire section below is just used to generate the PDF responses template at the end of the document. I copy and pasted it from "zadani" template provided by GravityPDF */
    .row-separator {
        clear: both;
        padding: 1.25mm 0;
    }

    .gf_left_half,
    .gf_left_third, .gf_middle_third,
    .gf_first_quarter, .gf_second_quarter, .gf_third_quarter,
    .gf_list_2col li, .gf_list_3col li, .gf_list_4col li, .gf_list_5col li {
        float: left;
    }

    .gf_right_half,
    .gf_right_third,
    .gf_fourth_quarter {
        float: right;
    }

    .gf_left_half, .gf_right_half,
    .gf_list_2col li {
        width: 49%;
    }

    .gf_left_third, .gf_middle_third, .gf_right_third,
    .gf_list_3col li {
        width: 32.3%;
    }

    .gf_first_quarter, .gf_second_quarter, .gf_third_quarter, .gf_fourth_quarter {
        width: 24%;
    }

    .gf_list_4col li {
        width: 24%;
    }

    .gf_list_5col li {
        width: 19%;
    }

    .gf_left_half, .gf_right_half {
        padding-right: 1%;
    }

    .gf_left_third, .gf_middle_third, .gf_right_third {
        padding-right: 1.505%;
    }

    .gf_first_quarter, .gf_second_quarter, .gf_third_quarter, .gf_fourth_quarter {
        padding-right: 1.333%;
    }

    .gf_right_half, .gf_right_third, .gf_fourth_quarter {
        padding-right: 0;
    }

    /* Don't double float the list items if already floated (mPDF does not support this ) */
    .gf_left_half li, .gf_right_half li,
    .gf_left_third li, .gf_middle_third li, .gf_right_third li {
        width: 100% !important;
        float: none !important;
    }

    /*
     * Headings
     */
    h3 {
        margin: 1.5mm 0 0.5mm;
        padding: 0;
    }

    /*
     * Quiz Style Support
     */
    .gquiz-field {
        color: #666;
    }

    .gquiz-correct-choice {
        font-weight: bold;
        color: black;
    }

    .gf-quiz-img {
        padding-left: 5px !important;
        vertical-align: middle;
    }

    /*
     * Survey Style Support
     */
    .gsurvey-likert-choice-label {
        padding: 4px;
    }

    .gsurvey-likert-choice, .gsurvey-likert-choice-label {
        text-align: center;
    }

    /*
     * Terms of Service (Gravity Perks) Support
     */
    .terms-of-service-agreement {
        padding-top: 3px;
        font-weight: bold;
    }

    .terms-of-service-tick {
        font-size: 150%;
    }

    /*
     * List Support
     */
    ul, ol {
        margin: 0;
        padding-left: 1mm;
        padding-right: 1mm;
    }

    li {
        margin: 0;
        padding: 0;
        list-style-position: inside;
    }

    /*
     * Header / Footer
     */
    .alignleft {
        float: left;
    }

    .alignright {
        float: right;
    }

    .aligncenter {
        text-align: center;
    }

    p.alignleft {
        text-align: left;
        float: none;
    }

    p.alignright {
        text-align: right;
        float: none;
    }

    /*
     * Independant Template Styles
     */
    .gfpdf-field .label {
        text-transform: uppercase;
        font-size: 90%;
    }

    .gfpdf-field .value {
        border: 1px solid <?php echo $value_border_colour; ?>;
        padding: 1.5mm 2mm;
    }

    .products-title-container, .products-container {
        padding: 0;
    }

    .products-title-container h3 {
        margin-bottom: -0.5mm;
    }

</style>

<p><h0>Baby Sleep Answers - Custom Sleep Plan for <?= $babyname_3; ?></h0></p>
<p>Hello <?= $fname_31_first; ?>!</p>
<p>Welcome to Baby Sleep Answers, we’ll get your baby sleeping better very soon!</p>

<p>It looks like <?= $babyname_3; ?> is very reliant on help to fall asleep, which is very common. You are definitely not alone! Unfortunately it is also one of the reasons <?= $lowHeOrShe_15; ?> is still not sleeping through the night. The focus of this plan is, therefore, to teach <?= $babyname_3; ?> how to sleep without your help. In this plan you’ll find some areas that need some readjusting. These changes, if followed with love and consistency will allow <?= $lowHimOrHer_15; ?> to sleep through the night without any unnecessary wakeups very soon!</p>

<p>Please take your time reading through this plan. It is shorter than a book you would find at a store but it is personalized to your specific case.</p>

<p>Something important to note right now is that you are TIRED and you may be experiencing some anxiety around baby sleep. I urge you to breathe in and out, remind yourself this is just a phase, and to talk to your baby through this process. Remember, <?= $babyname_3; ?> can feel your strong emotions! The more positive you are about this process, the more relaxed <?= $lowHeOrShe_15; ?> will feel.</p>
<pagebreak></pagebreak>
<p><h2>Why is Quality Sleep Important?</h2></p>

<p>Driving with a sleep deficit is comparable to driving after having a few drinks: sleep deprivation affects our brains directly. Your amygdala, which controls emotions, for example, works 60% more when you are sleep deprived. This means your emotions come at you at full speed! At the same time your medial frontal cortex, the amygdala regulator, doesn’t function to its full potential without sleep. How’s that for a combination? Just to really make a point: the hippocampus, where we store our memory, doesn’t work properly without enough sleep.</p>

<p>All to say, if you feel like you have eternal mommy brain: it doesn’t have to be this way!</p>

<p>On a more positive note, the benefits of quality sleep for a baby are countless! Not only are babies less likely to develop diabetes and be overweight as children and teenagers, a baby that sleeps well is also more likely to be in a better mood during the day. Babies that sleep enough also have more periods of optimal wakefulness which helps with learning and memory! And more!</p>

<p>Keep all of these benefits in mind as you start this journey towards independent sleep for <?= $babyname_3; ?>. They will work as great motivators for the tough nights to come. Any time you hear <?= $babyname_3; ?> cry remember that you are doing this for <?= $lowHimOrHer_15; ?>, for <?= $lowHisOrHer_15; ?> quality of life! And fortunately, if you follow this plan with love and consistency, it won’t be that much crying: definitely not hours of <?= $lowHimOrHer_15; ?> in the crib by <?= $lowHimselfOrHerself_15; ?>! We will just have to take it slowly and with care.</p>

<p>Let’s start with some baby sleep science!</p>

<p><h2>Why Should We Avoid Sleep Props?</h2></p>

<p>Because no one sleeps through the night!</p>

<p>Even adults wake up numerous times a night, but most of us don’t even acknowledge it since we are such good sleepers (or WERE before baby) and the wakeups are so brief. I’ve had many frustrated parents

    <?php if ( $sleepingthroughthenight_34 == 'Yes'): ?>
        , like yourself,
    <?php endif; ?>

     who tell me, “my baby USED to sleep through the night, but then it all went away!” This is because newborn sleep changes to more adult-like REM cycles around 3-4 months. After this change, which is commonly known as the 4-month sleep regression, babies start to go through sleep cycles that start with very light sleep stages and end with deeper sleep and REM sleep. Babies begin to wake up slightly every time they transition into a new cycle of sleep stages. If a baby doesn’t know how to do this on their own, well, they’re going to ask for help.</p> 

<p>As adults, we usually sleep all night in the same bed we fall asleep on; when we wake up slightly in the middle of the night after a sleep cycle we usually don’t even notice. This is when we may fluff up our pillow or pull back our covers... we subconsciously check our area and surroundings and know that they haven’t changed. This is why we (for the most part) are good at sleeping through the night!</p>

<p>Now... imagine you fell asleep with your favorite pillow (just the right amount of fluff!) and your room was at the perfect temperature. A few hours later it’s 12 am, you briefly wake up and realize that your pillow is missing and it is FREEZING! It would probably be hard for you to just turn around and go right back to sleep without getting your pillow back and adjusting the thermostat or rearranging the covers. If the pillow has fallen you can probably just reach down for it, if you know someone took in then you'll ask for it (or just pull it right back)... and then fall right back to sleep.</p> 

<p>Well, it’s the same for <?= $babyname_3; ?>! If <?= $lowHeOrShe_15; ?> is used to 

    <?php $i = 0;
            $ilen = count( $sleepcrutches_25 );
            foreach( $sleepcrutches_25 as $key => $value )
            {
                if( ++$i == $ilen ) {echo "$value";
                break;
                }
               echo "$value/";
            }
    ?>

    to fall asleep, well then when <?= $lowHeOrShe_15; ?> will ask for it whenever <?= $lowHeOrShe_15; ?> wants to go back to sleep!</p>

	<p class= "tab">“Hey! Help me, I need to be       
	    
	    
	    
    	<?php 
    	
    	/* loop through user selection and echo each value but do something different on the last value. In this case we do a "/" after each variable but the last one does not have one */
    	
    	$i = 0;
            $ilen = count( $sleepcrutches_25 );
            foreach( $sleepcrutches_25 as $key => $value )
            {
                if( ++$i == $ilen ) {echo "$value";
                break;
                }
               echo "$value/";
            }
        ?>
        
         to fall asleep! I want to go back to sleep!”</p>

<p>Unfortunately for you, <?= $lowHeOrShe_15; ?> doesn’t talk yet and so it may sound more like screaming and crying. This crying doesn’t translate to “You’re abandoning me! You’re the worst parent in the world!” It is just a cry telling you, “Hey, let’s be real: the only way I know how to fall asleep is by 

    <?php $i = 0;
            $ilen = count( $sleepcrutches_25 );
            foreach( $sleepcrutches_25 as $key => $value )
            {
                if( ++$i == $ilen ) {echo "$value";
                break;
                }
               echo "$value/";
            }
    ?>
    
    so let’s do that and we can all go back to sleep.”</p>

<p><h2>So What Do I Do?</h2></p>
<p>We need to teach <?= $lowHimOrHer_15; ?> how to fall asleep on <?= $lowHisOrHer_15; ?> own. The way I like to do it is to first learn this skill at bedtime, and then <?= $lowHeOrShe_15; ?> will be able to transfer this skill to the middle of the night and nap sleeps as well. The reason I sleep train this way is that homeostatic pressure (a push to sleep) is higher around bedtime. Bedtime is also easier to figure out than naptime- and sometimes, depending on how much crying is involved in the method you choose, it’s simply easier to deal with it at bedtime than in the middle of the night or day. If you'd like to do it differently, by starting with all sleeps for example, or by just focusing on naps first, then that's up to you! According to sleep psychologist Jodi Mindell, it doesn't make that big of a difference.</p>
<p><h1>Our Goal:</h1></p>

<p>The goal you picked was:</p>
<p class= "tab">    

    <?php if (is_array( $form_data['field'][16] ) ) {
        echo implode( '<br>', $form_data['field'][16] ); /* output name of each selected item */
        }
    ?>
</p>
<p>This is achievable! Keep in mind that sleeping through the night looks different for different babies of varying ages.</p> 
<p> At <?= $age_5_months; ?> months old, 
    <?php if ( $age_5_months <= 7 ): ?> 5 to 8 hours could be considered sleeping through the night.
    <?php endif; ?>
    <?php if ( $age5_months >= 8 ): ?> anything more than 8 hours could be considered sleeping through the night.
    <?php endif; ?> </p>
    
<p>My goal for you is twofold:</p>

<p>1) to get to know your baby better. This is crucial! This is the biggest key to the puzzle of sleep (and it's fun!) This will help with scheduling, with figuring out the perfect bedtime, and knowing when baby really needs you or when they can figure sleep out on their own. And 2) to get the most sleep possible. For both you AND baby!</p>

<p>The following plan will help you achieve all of our goals if you follow it with love and consistency.</p> 

<p>We will start by creating the ideal schedule for <?= $babyname_3; ?>. A better-rested baby will sleep better! And so that will be our first step:</p>

<p><h1>Schedule</h1></p>
<p><h2>Flexible but Consistent Schedule:</h2></p>

<p>If that sounds confusing... please bear with me ;).</p> 

<p>Babies THRIVE on consistency; they are able to expect certain things and react appropriately when they have consistency. So we want to be able to provide this for <?= $babyname_3; ?>.</p> 

<p>As we begin to sleep train we will first make sure that <?= $lowHeOrShe_15; ?> is not suffering from overtiredness, otherwise, we can end up with many hours of unnecessary crying. To do this we first have to look at <?= $lowHisOrHer_15; ?> waketimes. The “waketime,” or time awake between sleep, is based on <?= $babyname_3; ?>'s age of <?= $age_5_months; ?> months. Right now <?= $lowHisOrHer_15; ?> waketime should
  <?php if ( $age_5_months <= 4 ): ?> be 2 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months == 5 ): ?> be 2.25 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months == 6 ): ?> be 2.5 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months == 7 ): ?> be 2.75 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months == 8 ): ?> be 3 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months == 9 ): ?> be 3 - 3.5 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months == 10 ): ?> be 3.5 - 4 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months == 11 ): ?> be 3.5 - 5 hours on average, 
  <?php endif; ?>
  <?php if ( $age_5_months >= 12 and $age5_months < 18): ?> depend on how many naps they take: 4 - 5 hours if they take 2 naps, and 4.5 - 6 if they take 1 nap, 
  <?php endif; ?>
  <?php if ( $age5_months >= 18 ): ?> be 5-7 hours on average, 
  <?php endif; ?>
before being tired for bed: we will have to hone into <?= $lowHisOrHer_15; ?> sleep cues and find optimal waketimes to see when <?= $lowHeOrShe_15; ?> will be the perfect amount of sleepy to best learn independent sleep.</p> 
<p>Check out this chart for future waketimes and see how it changes throughout <?= $lowHisOrHer_15; ?> babylife:</p>

    <table id="t01">
     <tr>
      <th>Age</th>
      <th>Waketime</th> 
      <th>Naps</th>
      <th>Nap Duration</th>
      <th>Bedtime</th> 
      <th>Night Sleep</th>
     </tr>
    <tr>
      <td>1 month</td>
      <td>45 min - 1 hr</td>
      <td>4+</td>
      <td>30min-3hr</td>
      <td>9-11pm</td>
      <td>8-14hr</td>
    </tr>
    <tr>
      <td>2 months</td>
      <td>1.25 hr</td>
      <td>4+</td>
      <td>30min-2hr</td>
      <td>8-11pm</td>
      <td>8-14hr</td>
    </tr>
    <tr>
      <td>3 months</td>
      <td>1.5 hr</td>
      <td>3-4</td>
      <td>30min-2hr</td>
      <td>8-10pm</td>
      <td>8-13hr</td>
    </tr>  
    <tr>
      <td>4 months</td>
      <td>2 hr</td>
      <td>3-4</td>
      <td>30min-2hr</td>
      <td>8-10pm</td>
      <td>8-13hr</td>
    </tr>
    <tr>
      <td>5 months</td>
      <td>2.25 hr</td>
      <td>3</td>
      <td>1-2hr</td>
      <td>7-10pm</td>
      <td>9-12hr</td>
    </tr>
    <tr>
      <td>6 months</td>
      <td>2.5 hr</td>
      <td>2-3</td>
      <td>1-2hr</td>
      <td>7-9pm</td>
      <td>9-12hr</td>
    </tr>
    <tr>
      <td>7 months</td>
      <td>2.75 hr</td>
      <td>2-3</td>
      <td>1-2hr</td>
      <td>7-9pm</td>
      <td>9-12hr</td>
    </tr>
    <tr>
      <td>8 months</td>
      <td>3 hr</td>
      <td>2-3</td>
      <td>1-2hr</td>
      <td>7-8pm</td>
      <td>9-12hr</td>
    </tr>
    <tr>
      <td>9 months</td>
      <td>3-3.5 hr</td>
      <td>2</td>
      <td>1-2hr</td>
      <td>7-8pm</td>
      <td>10-12hr</td>
    </tr>
    <tr>
      <td>10 months</td>
      <td>3.5-4 hr</td>
      <td>2</td>
      <td>1-2hr</td>
      <td>7-8pm</td>
      <td>10-12hr</td>
    </tr>
    <tr>
      <td>11 months</td>
      <td>3.5-5 hr</td>
      <td>1-2</td>
      <td>1-2hr</td>
      <td>7-8pm</td>
      <td>10-12hr</td>
    </tr>
    <tr>
      <td>12-18 months<br>12-18 months</td>
      <td>4-5 hr<br>4.5-6 hr</td>
      <td>2<br>1</td>
      <td>1-3hr</td>
      <td>7-8pm</td>
      <td>10-12hr</td>
    </tr>
    <tr>
      <td>18 months</td>
      <td>5-7 hr</td>
      <td>1</td>
      <td>1-3hr</td>
      <td>7-8pm</td>
      <td>10-12hr</td>
    </tr>
    </table>

<p>(NOTE: Waketimes are more of a guideline- if you’ve been putting <?= $lowHimOrHer_15; ?> down according to sleep cues and have good long naps and good night sleep, keep doing that throughout, careful not to exceed waketimes by more than 30-60 minutes to avoid overtiredness.)</p>

<p>Following these waketimes will help <?= $babyname_3; ?> be tired enough for sleep and never be overtired. Remember that overtired is our worst enemy. When a baby is awake for too long it is harder for them to fall asleep, especially on their own. Their brain goes on overdrive, they get a full dose of adrenaline and cortisol, which means they get a second wind and even calming down is too difficult in the middle of the night, and they also wake up more mid sleep! </p>

<p>I’ll repeat this numerous times but it’s the most important part of babysleep: consistency. Being consistent with waketimes will be one of your secret weapons in getting the best sleep for <?= $babyname_3; ?>.</p>

<p><h2>So what does a sample schedule look like at this age?</h2></p>

<p><b>Sample Schedule</b></p>
    <section class = "tab">
    <?php if ( $age_5_months <= 4 ): ?> 
    <p>For 4 month old babies we want to go through the day looking at waketimes and sleep cues. Below are sample flexible schedules for <?= $babyname_3; ?>:</p>
    <p>On Three naps (Two long, one short)</p>
    <p>7am awake for the day</p>
    <p>9:00-10:30 nap #1</p>
    <p>12:30 - 2:00 nap #2</p>
    <p>4:30-5 nap #3</p>
    <p>7-7:30pm bedtime</p> 
        
    <p>If on 4 short naps then more like:</p> 
    <p>7am awake for the day</p>
    <p>9:00-9:45 nap #1</p>
    <p>11:45-12:30 nap #2</p>
    <p>2:30-3:15 nap #3</p>
        
    <p>Forced short nap (15-30 minutes) around 4:30/5pm to make it to bedtime without getting overtired</p>
        
    <p>7/7:30 bedtime</p>
    <?php endif; ?>
      
    <?php if ( $age_5_months == 5 ): ?> 
    <p>For <?= $babyname_3; ?> we want to go through the day looking at waketimes and sleep cues. Below is two sample flexible schedules:
    <p>
    7am awake for the day
    </p><p>
    9:00-10:30 nap #1</p><p>
    12:45 -2:00 nap #2</p><p>
    4:30-5 nap #3</p><p>
    7-7:30pm bedtime</p><p>
    </p><p>
    If naps are still short then more like:</p><p> 
    7am awake for the day</p><p>
    9:00-9:45 nap #1</p><p>
    11:45-12:30 nap #2</p><p>
    2:30-3:15 nap #3</p><p>
    </p><p>
    Forced short nap (15-30 minutes) around 5pm to make it to bedtime</p><p> 
    </p><p>
    7/7:30 bedtime
    </p>
  <?php endif; ?>
  
  <?php if ( $age_5_months >= 6 and $age_5_months <= 8 ): ?> 
    <p>For <?= $babyname_3; ?> we want to go through the day looking at waketimes and sleep cues. At some point between 6-8 months most babies drop down to two naps. Below are two sample schedules for 3 naps and one for two naps:</p><p> 
    </p><p>
    7am awake for the day</p><p>
    9:00-10:30 nap #1</p><p>
    12:45 -2:00 nap #2</p><p>
    4:30-5 nap #3</p><p>
    7-7:30pm bedtime</p><p>
    </p><p>
    If naps are still short then more like: </p><p>
    7am awake for the day</p><p>
    9:00-9:45 nap #1</p><p>
    11:45-12:30 nap #2</p><p>
    3:30-4:00 nap #3</p><p>
    </p><p>
    6:30/7 bedtime</p><p>
    </p><p>
    Forced short nap (15-30 minutes) around 5pm to make it to bedtime without too much waketime.</p><p>
    </p><p>
    For 2 naps: </p><p>
    7am awake for the day</p><p>
    10:00-11:20 nap #1</p><p>
    2:20-3:00 nap #2</p><p>
    7/7:30pm bedtime</p><p>
    </p>
  <?php endif; ?>
  
  <?php if ( $age_5_months == 9 ): ?> 
    <p>Most 9 month old babies need two long naps throughout the day. For this you can either go by waketime or do the 2-3-4 schedule, I’ll write both of those options down</p><p>
    </p><p>
    7am awake for the day</p><p>
    10:00-11:20 nap #1</p><p>
    2:20-3:00 nap #2</p><p>
    7/7:30pm bedtime</p><p>
    </p><p>
    OR</p><p>
    </p><p>
    7 am awake for the day</p><p>
    9-11 nap #1</p><p>
    2-3 nap #2</p><p>
    7pm bedtime</p><p>
    </p>
  <?php endif; ?>
  
  <?php if ( $age_5_months == 10 ): ?>
    <p>For most 10 month old babies you can either go by waketime or do the 2-3-4 schedule, I’ll write both of those options down</p><p>
    </p><p>
    7am awake for the day</p><p>
    10:00-11:20 nap #1</p><p>
    2:20-3:00 nap #2</p><p>
    7/7:30pm bedtime</p><p>
    </p><p>
    OR
    </p><p>
    7 am awake for the day</p><p>
    9-11 nap #1</p><p>
    2-3 nap #2</p><p>
    7pm bedtime</p><p>
    </p><p>
      
    </p>
  <?php endif; ?>
  
  <?php if ( $age_5_months >= 11 ): ?>
    <p> For <?= $babyname_3; ?>, since <?= $lowHeOrShe_15; ?>’s older than 10 months you can either go by waketime or do the 2-3-4 schedule until <?= $lowHeOrShe_15; ?>’s ready to drop to one nap. Below are both of those options, as well as a one nap schedule for when that happens.</p><p>
      </p><p>
      7am awake for the day</p><p>
      10:00-11:20 nap #1</p><p>
      2:20-3:00 nap #2</p><p>
      7/7:30pm bedtime</p><p>
      </p><p>
      OR</p><p>
      </p><p>
      7 am awake for the day</p><p>
      9-11 nap #1</p><p>
      2-3 nap #2</p><p>
      7pm bedtime</p><p>
      </p><p>
      Sample for a 1 nap schedule:</p><p> 
      </p><p>				
      Wake up at 6:30/7am Nap at 12:30-2pm Bedtime at 7/7:30pm</p><p>
    </p>
  <?php endif; ?>
  </section>
  
  <p>Note that if naps are short, that’s normal as we start to sleep train - we may want to lengthen them if baby is cranky and unhappy in general, and that is fine. But know that short naps before sleep training doesn’t mean you’re doing everything wrong. Once <?= $lowHeOrShe_15; ?> is sleep trained, then you can focus on lengthening naps.</p><p>
  </p>
  <p>We also want to aim for bedtime about 12-13 hours after morning wakeup time, that will be a good guideline for you to figure out bedtime.</p>
  
  <p><u>Flexibility</u>
  </p><p>
  If you follow me on instagram you’ll know I’m all about personalization, about being flexible and kind to ourselves and our babies- understanding that they are just little humans with their own unique needs that are in no way STRICT and INFLEXIBLE. That is why their schedule will always vary at least a little bit. What I mean by that is that we will have to be in tune with them every day, finding <?= $lowHisOrHer_15; ?> perfect waketime by seeing <?= $lowHisOrHer_15; ?> their mood and sleep cues. This isn’t as hard as it sounds, and the better attuned we are to <?= $lowHisOrHer_15; ?> schedule, the easier and less changing it will be.</p><p>
  </p><p>
  <h2>About Sleep Cues</h2></p>
  <p>There is a common misconception about sleep cues, unfortunately. Most people think that when baby starts to yawn or begins to rub their eyes that means they’re starting to get tired. Those cues actually mean baby is already VERY tired. They should be in their crib at this point.</p><p> 
  </p><p><b>The best sleep cue to look for is the redness under their eyes. When a baby starts to get tired (unless they have been crying and this caused redness) their lower eyelid will start reddening.</b> THIS is what you’ll be looking for once you start having <?= $lowHimOrHer_15; ?> fall asleep by <?= $lowHimselfOrHerself_15; ?>. This and the slowing down of limb and eye movement. Once you see these cues you will know it’s time to start the nap routine (see below).</p><p> 
  </p><p>
      Having a general schedule/window for naps around the same time every day will help <?= $babyname_3; ?>’s body settle into a better sleep rhythm for the most optimal sleep. <?= $lowHisOrHer_15; ?>own schedule will start to emerge as you follow <?= $lowHisOrHer_15; ?> sleep cues and give <?= $lowHimOrHer_15; ?> appropriate waketimes in appropriate environments. You won’t have to be watching <?= $lowHimOrHer_15; ?> as intensely. If you can commit to waking <?= $lowHimOrHer_15; ?> up at the same time every day, this will make scheduling a lot easier. Once <?= $lowHeOrShe_15; ?>’s on a better nap schedule, you can let <?= $lowHimOrHer_15; ?> wake up on <?= $lowHisOrHer_15; ?> own.</p><p> 
  </p><p>
      As I said above, I always suggest you nap train only after a few nights (or weeks) of bedtime sleep training. The important thing in starting to sleep train is that <?= $lowHeOrShe_15; ?> gets <?= $lowHisOrHer_15; ?> naps, it doesn’t matter how. Maybe by 
      
            <?php $i = 0;
                    $ilen = count( $sleepcrutches_25 );
                    foreach( $sleepcrutches_25 as $key => $value )
                    {
                        if( ++$i == $ilen ) {echo "$value";
                        break;
                        }
                       echo "$value/";
                    }
            ?>
      
      … as long as <?= $lowHeOrShe_15; ?>’s sleeping, you’re good! I’ve honestly spent a lot of hours with a baby on my lap or chest as I type up sleep plans or sleep consults, after my baby nursed himself out.</p><p> 
  </p><p>
  <h1>Naps</h1></p><p>
  </p><p>
      <h2>Unwanted bad naps</h2></p><p>
  </p><p>
      When sleeptraining, you have to be the <i>nap guardian</i>. If <?= $babyname_3; ?> falls asleep 5 minutes while eating unfortunately that counts as a nap. If <?= $lowHeOrShe_15; ?> falls asleep for 15 minutes in the car, this also counts as a nap unfortunately. They will mess with how <?= $lowHeOrShe_15; ?> is able to sleep the rest of the day (and night). Be careful not to let  <?= $lowHimOrHer_15; ?> take these kinds of short, short naps (when possible, of course) so that they don’t interfere with <?= $lowHisOrHer_15; ?> emerging nap schedule.</p><p>
  </p></p><p>
     <h2>Fighting Naptime</h2></b>
</p><p>
    If <?= $babyname_3; ?> ever starts hollering and screaming as you place <?= $lowHimOrHer_15; ?> in <?= $lowHimOrHer_15; ?> crib, it may be that <?= $lowHeOrShe_15; ?> is not tired enough or that <?= $lowHeOrShe_15; ?> is overtired. At this point you have to just start gauging it. If <?= $lowHisOrHer_15; ?> wake time has exceeded 
            <?php if ( $age_5_months <= 4 ): ?>
                2.5  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months == 5 ): ?>
                2.75  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months == 6 ): ?>
                3  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months == 7 ): ?>
                3.25  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months == 8 ): ?>
                3.5  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months == 9 ): ?>
                3.5 - 4  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months == 10 ): ?>
                4 - 4.5  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months == 11 ): ?>
                4 - 5.5  hours,
            <?php endif; ?>
            
            <?php if ( $age_5_months >= 12 and $age_5_months <=18 ): ?>
                5.5 or 6.5 hours (depending on two or one naps),
            <?php endif; ?>
        
            <?php if ( $age_5_months > 18 ): ?>
                5.5 - 7.5
            <?php endif; ?>
            
        then it is most likely overtiredness and <?= $lowHeOrShe_15; ?> will need more help getting to sleep. At this point I always just get my baby in my car or stroller to make SURE he sleeps before it gets too close to his bedtime. If <?= $babyname_3; ?> is not tired enough, then wait a little longer, play with <?= $lowHimOrHer_15; ?> more excitedly, and then put <?= $lowHimOrHer_15; ?> down again. </p><p>
    
    <?php if ( $age_5_months <= 7 and $numberofnaps_24 == '3+' ): ?>
                </p><p>
                <h2>The Last Nap of The Day</h2></p><p>
                    
                The last nap of the day will always be super rough, especially for some babies – sounds like <?= $babyname_3; ?> may be one of these kiddos. For this last nap keep taking it with <?= $lowHimOrHer_15; ?> , by nursing, rocking, bouncing in the dark, however you can get this nap even once you sleep and nap train. Don’t stress about sleep training for this nap! </p><p>
                </p>
    <?php endif; ?>

<p><h2>Naptime routine</h2></p><p>
This one didn’t come naturally to me at all. I had always heard of bedtime routine but for some reason I never thought of a naptime routine! Some babies don’t need one when they’re well sleep-trained, but for most babies, a naptime routine is KEY. 
    <?php if ( $isnaptimeroutine_19 == 'Yes'): ?>
        Great job having a routine before naptime, check my suggested one out and see how they compare!
    <?php else: ?>
        We have to come up with a quick 5-10 minute routine before naptime. It could be something very similar to the bedtime one but shorter, something very simple.
    <?php endif; ?>
</p><p>
 This is a sample naptime routine:</p><p>
-diaper change</p><p>
-sing a song rocking in a dimly lit room</p><p>
 -“it’s naptime <?= $babyname_3; ?>, I love you”</p><p>
-turn light off completely, cuddle her a few seconds</p><p>
-place <?= $babyname_3; ?> in <?= $lowHimOrHer_15; ?> crib while <?= $lowHeOrShe_15; ?> is still very awake.</p><p>
-leave the room.</p><p>
 </p><p>
     A naptime routine will provide consistency and these naptime cues will help <?= $lowHimOrHer_15; ?> body get ready to sleep well and longer. Ideally all naps should be in <?= $lowHimOrHer_15; ?> own crib. This world is obviously not ideal and that won’t be always a possibility, especially whenever we travel but try to do this whenever possible, especially when naptraining. For more routine help please look at the next section: Bedtime. </p><p>
 <b>NOTE: keep helping <?= $lowHimOrHer_15; ?> for naps for a while until <?= $lowHeOrShe_15; ?>’s going down by <?= $lowHimselfOrHerself_15; ?> }at nighttime or sleeping longer stretches at least, this will save <?= $lowHimOrHer_15; ?>  from overtiredness.</b></p><p>
</p>

    <?php if ( $age_5_months <= 5 ): ?>
        <p><h2>Eat Play Sleep</h2></p><p>
        Although this is popular advice given to moms, it does not always work to your advantage, especially if you’re feeding on demand. For me it was a source of anxiety, and later I realized this was a reason my baby was not sleeping long naps. If you always feed a baby right when they wake up they may start to think that the answer for waking up is a feed, and therefore ask for it, whether they are hungry or not. The reasoning behind this method is to always separate feeding from falling asleep, and this is great. Nursing or feeding to sleep is one of the hardest sleep props to break away from! But you can separate feeding to sleep simply by moving it away from being the last step before crib and you’ll be good.</p>
    <?php endif; ?>
<p>
</p>
<p>
<h1>Bedtime</h1></p><p>
<?php if ( $iswinddown_22 == 'No' ): ?>
        <p><h2>Winding Down Before Bedtime</h2></p><p>
        The hour before bedtime is surprisingly important to good sleep! A baby that spends this time overstimulated will have a harder time falling asleep on their own, and staying asleep, so make sure you have some down time even before the calm bedtime routine! </p><p>
        What are some examples of downtime? </p><p>
            -Reading books</p><p>
            -Singing</p><p>
            -Praying as a family</p><p>
            -Looking at high contrast images</p><p>
            -Listening to classical music</p><p>
            -anything that isn’t too stimulating!</p><p>
    <?php endif; ?>
</p><h2>Bedtime Time</h2>
An optimal bedtime for infants is around 7-8pm. Babies sleep more restoratively at nighttime, so it’s important to get their bedtime started around 7:30 at the latest. Once we have worked on a sleep training method the routine should only last 15-20 minutes. In the first few weeks of sleep training this will be crucial. Giving <?= $babyname_3; ?> new good sleep associations when taking away the biggest association <?= $lowHeOrShe_15; ?> ’s had for the past months will mean the world. We want to work on making it consistent at a specific time between 7-8 and then slowly move it slightly earlier if needed.</p><p> 
</p><p>
<h2>Bedtime routine</h2></p><p>
Baby bodies (and toddler and kid… and adult bodies) actually LOVE routines. We may fight the implementation of routines (because who doesn’t?) but once routines are set, the consistency is so comforting. Consistency, consistency, consistency is key! They are helpful mostly because when out of the normal events occur, or when new things (Hey I can walk? Hey I’m in a new room? Hey I have a sibling??) pop up, they provide a comfort that all babies really crave and need.</p><p>
You should aim for particular actions that you do the minutes before putting <?= $lowHimOrHer_15; ?> down so that<?= $lowHimOrHer_15; ?> whole body receives hints to know it’s time to rest. Make sure the routine includes the following:</p><p>
 • A dimly lit room.</p><p>
• A change of outfit (and diaper) to signal bedtime</p><p>
 • A quiet, interactive but not overly engaging activity</p><p>
 o Ie reading, singing, praying</p><p>
• A loving gesture/comforting affirmation of love</p><p>
o Ie kisses and hugs, some cuddling</p><p>
 • Lasts only 15-20 minutes</p><p>
</p><p>
Notice how feeding in general is not a part of the routine. In order to teach <?= $lowHimOrHer_15; ?> independent sleep, without a feeding association, we need to separate milk from the bedtime routine. Ideally you should nurse <?= $lowHimOrHer_15; ?> outside the bedroom, in a very lit room, with a lot of noise so there’s no chance <?= $lowHeOrShe_15; ?> ’ll fall asleep. Once you know <?= $lowHeOrShe_15; ?>’s full, you start the routine in the darkened room and place <?= $lowHimOrHer_15; ?> down. (I go over sleep training methods below.) This way <?= $lowHeOrShe_15; ?> will be falling asleep entirely on <?= $lowHimOrHer_15; ?> own and learn to transfer that to middle-of-the-night-wakings.</p><p>
</p><p>
Bathtime is also not in the routine. Babies generally don’t consider anything outside of the dimly lit room to be a part of routine. If you want a bath before bedtime (I love that aspect of our night routine with our toddler) then do it, but don’t worry when bath is not a possibility. </p><p>
</p><p>
<h2>Bedroom environment</h2></p><p>
 An ideal bedroom environment is pitch black (perhaps a little light if you see <?= $lowHeOrShe_15; ?> needs it – but this is very rare until they’re older toddlers), white noise at an appropriate level (85 decibels at the most, and not right next to <?= $lowHimOrHer_15; ?> ) and a cooler temperature between 70-72 F. Darkness helps our body shut down and if <?= $lowHeOrShe_15; ?> can see any light this can make <?= $lowHimOrHer_15; ?> self-sleep capabilities a little harder at any waketime. Having black out curtains definitely helps if the sun is still up at night or during the summer days when the sun wakes up before we want to. Our bodies react very strongly to sunlight, we are circadian creatures- so the more you help <?= $babyname_3; ?> receive sun when you want <?= $lowHimOrHer_15; ?> to be awake and be covered from it when you want to signal bedtime, the better. It’s crucial that you try to get the room as dark as you can for sleep training, even using tinfoil haha!</p><p>
</p><p>
<h2>Placing <?= $lowHimOrHer_15; ?> Down in the Crib</h2></p><p>
After the routine is over and you have told <?= $babyname_3; ?> you love <?= $lowHimOrHer_15; ?> , simply place <?= $lowHimOrHer_15; ?> down in the crib, whilst <?= $lowHeOrShe_15; ?> is still awake (see below for sleep training methods if you need to use them in <?= $lowHimOrHer_15; ?> crib).</p><p>
The first time you do this, it may shock <?= $lowHimOrHer_15; ?> . You can stay with <?= $lowHimOrHer_15; ?> and calm <?= $lowHimOrHer_15; ?> down (see sleep training methods below for more help). The important thing is to not let <?= $lowHimOrHer_15; ?> fall asleep on you and not to fall back on 

        <?php $i = 0;
                $ilen = count( $sleepcrutches_25 );
                foreach( $sleepcrutches_25 as $key => $value )
                {
                    if( ++$i == $ilen ) {echo "$value";
                    break;
                    }
                   echo "$value/";
                }
        ?>

in order to get <?= $lowHimOrHer_15; ?> to sleep. </p><p>
</p><p>
<h2>Drowsy but Awake</h2></p><p>
	If you’ve done any sleep training research you have probably ran into this phrase “drowsy but awake.” It is misnamed in my opinion. It should be ‘sleepy but awake.’ When you put <?= $babyname_3; ?> down in <?= $lowHimOrHer_15; ?> crib you have to make sure <?= $lowHeOrShe_15; ?> is ready for bed. That <?= $lowHimOrHer_15; ?> limb and eye movement is slower, <?= $lowHimOrHer_15; ?> energy has taken a drop, <?= $lowHeOrShe_15; ?> will rest <?= $lowHimOrHer_15; ?> head on your shoulder if you carry <?= $lowHimOrHer_15; ?>, etc… but <?= $lowHeOrShe_15; ?> is not what we would call ‘drowsy.’ If you see your baby’s eyes closing and opening, this is the first stage of sleep!</p><p>
This is why many moms will ‘swear’ they put baby down awake but the moment they let go of <?= $lowHimOrHer_15; ?> <?= $lowHeOrShe_15; ?> starts crying! This may have happened to you before. It’s because baby was happily falling asleep on mom and then they were placed on a cold empty mattress, which woke them up! I wouldn’t be too happy...</p><p>
 So if you fed and burped before the routine, went through the routine consistently, had a great day schedule, and made sure <?= $lowHeOrShe_15; ?> has not fallen asleep before <?= $lowHeOrShe_15; ?> is in the crib at an appropriate time, the crying and/or screaming should go away within a few nights. So all to say: make sure <?= $lowHeOrShe_15; ?> is AWAKE when you put <?= $lowHimOrHer_15; ?> in <?= $lowHimOrHer_15; ?> crib. This way when <?= $lowHeOrShe_15; ?> wakes up in the middle of the night <?= $lowHeOrShe_15; ?> can just do the same thing: go back to sleep.</p><p> 
</p><p>
<h2>Sleep Time</h2></p><p>
    On average, all humans take between 10-20 minutes to fall asleep. If you fall asleep a lot faster it means you are overtired- same goes for babies. So if <?= $babyname_3; ?> moves around once you place <?= $lowHimOrHer_15; ?> down, this is more than normal! <?= $lowHeOrShe_15; ?> is trying to find a comfortable position to fall asleep for the night. Do your best not to help <?= $lowHimOrHer_15; ?> (if <?= $lowHeOrShe_15; ?> is not screaming), and just exit the room to let <?= $lowHimOrHer_15; ?> find <?= $lowHimOrHer_15; ?> way on <?= $lowHimOrHer_15; ?> own. Now… what if <?= $lowHeOrShe_15; ?> starts crying? (And let’s be honest, <?= $lowHeOrShe_15; ?> most likely will start crying) Let’s get on to the sleep training methods!</p><p>
</p><p>
<h1>Disclaimer about Tears</h1></p><p>
<b>Will you be leaving your baby to “Cry it Out?”</b> The simple answer is “no.” I am not an advocate of Cry it Out. Not because it damages a baby, there are actually many, many studies that prove Cry it Out does not affect a baby in the long run. I’m not an advocate of it because it’s simply not something I can do as a mom. I believe bedtime needs to be a very happy, pleasant experience, not something that causes us anxiety. I don’t like to recommend anything I wouldn’t do, but if that is something you’re interested in I can definitely coach you through it, just let me know! In no way do I look down on it, in fact I think it is way easier than gentle methods with way quicker results.</p><p>
The more complicated answer is, “there will be a some tears.” It would be unreasonable to believe there would be no tears in a process that is breaking habits. Humans are stubborn creatures of habits, and when we’re trying to break a habit, we get upset. And that’s exactly what may happen to <?= $babyname_3; ?>. <?= $lowHeOrShe_15; ?> may be angry and <?= $lowHeOrShe_15; ?> will protest, but eventually <?= $lowHeOrShe_15; ?> will get over it and find a new way to fall asleep if given the opportunity. But <?= $lowHeOrShe_15; ?> has to be given this opportunity to learn!</p><p>
 </p><p>
(Mombit: My mother always says our greatest, most important, and toughest job as a parent is to never do for them what they can do for themselves, otherwise we are blocking their development and road to independence. This is HUGE with sleep.)</p><p>
</p><p>
I know from experience how heartbreaking a baby’s cries are. I’m a huge sucker for tears. We have to keep in mind, however, that once we have decided sleep training is needed, then what we are doing is what is best for baby. A baby that is well rested is gaining much more than just more sleep: a well rested baby can practice milestones and have fun during the day without being cranky or over tired. According to the National Sleep Foundation, “sleep is especially important for children, as it directly impacts physical and mental development.” The deep stages of sleep are when “blood supply to the muscles is increased, energy is restored, tissue growth and repair occur, and important hormones are released for growth and development.” </p><p>
</p><p>
So when you hear <?= $babyname_3; ?> cry out, remember that you are doing <?= $lowHimOrHer_15; ?> a huge favor, you are doing this not only for <?= $lowHimOrHer_15; ?> sleep and your mental stability but mainly for <?= $lowHimOrHer_15; ?> well-being long term. Crying is nothing more than protesting, and when dealt with consistency and love, it is usually short-lived. Once <?= $babyname_3; ?> realizes that you will no longer be putting <?= $lowHimOrHer_15; ?> to sleep at bedtime or in the middle of the night, the protesting and tears will stop and <?= $lowHeOrShe_15; ?> will be falling asleep on <?= $lowHimOrHer_15; ?> own.</p><p>
 Think of it like a grocery meltdown. If your toddler just HAS to have the shiny beer <?= $lowHeOrShe_15; ?> saw and you do not want to buy it for <?= $lowHimOrHer_15; ?> because, well, you’re not going to buy beer for him… do you just give it to <?= $lowHimOrHer_15; ?> since he’s crying so much for it? No of course not! <?= $lowHeOrShe_15; ?> may cry and kick and scream, and yet, you will not buy <?= $lowHimOrHer_15; ?> beer. <?= $lowHeOrShe_15; ?> is little; you are the adult that knows what is good for <?= $lowHimOrHer_15; ?>, not <?= $babyname_3; ?>.</p><p>
 Now, what if your toddler cried and screamed for 2 hours and then you decided to give <?= $lowHimOrHer_15; ?> a beer? That would send <?= $lowHimOrHer_15; ?> a mixed message. The most important thing from now on is to decide that you will NOT feed <?= $lowHimOrHer_15; ?> to sleep even if <?= $lowHeOrShe_15; ?> cries for a long time. We aren’t just ‘getting more sleep’ here, we are teaching a baby how to sleep on <?= $lowHimOrHer_15; ?> own.</p><p>
 </p><p>
<b>Note: do not start to sleep train on a day when you know <?= $babyname_3; ?> is overtired. Leaving a very overtired infant to figure out a new habit is much harder than teaching a perfectly tired baby. You will have a lot more crying if you try to do so. Same goes for putting <?= $lowHimOrHer_15; ?> down when <?= $lowHeOrShe_15; ?> is not ready for bed. This leads to overexcitement and a release of adrenaline and cortisol similar to the effects of being overtired. Additionally, overtiredness makes it harder for baby to transition in the last sleep cycles around 4/5 am, so we want to avoid overtiredness (Did I mention that already? ha!)</b></p><p>
</p><p>
<h1>The Plan</h1></p><p>
The plan is basic: teach <?= $babyname_3; ?> to fall asleep on <?= $lowHisOrHer_15; ?> own without needing to 

    <?php $i = 0;
            $ilen = count( $sleepcrutches_25 );
            foreach( $sleepcrutches_25 as $key => $value )
            {
                if( ++$i == $ilen ) {echo "$value";
                break;
                }
               echo "$value/";
            }
    ?>

. We will first do this at bedtime, then middle of the night wakings, and then naptime.</p><p>
It sounds like <?= $lowHeOrShe_15; ?> is also reliant on your presence to fall asleep for naps and that is an association we want to cut out eventually. We want to tackle bedtime and middle of the night first, as I have mentioned, and then move on to naps. There is the option of tackling both together but as I wrote above, sleep push is much greater at night, and naps are usually a bit trickier to tackle. Amazingly, babies are really good at distinguishing night sleep from day sleep habits. If you would like to go all out, however, it is up to you.</p><p> 
 When you start sleep training at night, you have a few things working for you that you don’t have during nap time. For one, baby will be tired from <?= $lowHimOrHer_15; ?> long day. Also we have our circadian rhythms that make it easier for us to fall asleep at night. We all have circadian rhythms that follow a 24- hour clock and control our physical, mental, and behavioral changes. The rhythms are influenced by the external environment around us, mainly the daylight and night's darkness. These external cues drive our internal clocks, and release more sleep hormones.</p><p> 

    <?php if ( $cosleep_23 == 'Yes move' ): ?>
        </p><p>
        <h2>Transition from Co-sleeping to Crib</h2></p><p>
         </p><p>
        Below are 3 different paths to use in the transition from co-sleeping to crib, going from most gradual to just straightforward. Honestly, I generally recommend the middle one if you can handle it, because it will be quick but not too drastic.</p><p>
        </p><p>
        These three methods assume that you want <?= $babyname_3; ?> in <?= $lowHimOrHer_15; ?> own room in <?= $lowHisOrHer_15; ?> own crib. If you want <?= $lowHimOrHer_15; ?> in a crib in your room or in a bed/floor mattress in their own room then just readjust the plans to fit that scenario.</p><p>
        </p><p>
        <u><h3>Most Gradual Plan for Co-sleeping to Crib</h3></p><p>
        Nights 1-2 Continue what you have been doing, but on a mattress next to <?= $lowHisOrHer_15; ?> crib, so <?= $lowHeOrShe_15; ?> 
         gets used to being near <?= $lowHisOrHer_15; ?> crib for bedtime.</p><p>
        Nights 3-4 Place <?= $lowHimOrHer_15; ?> in the crib for the first sleep cycle and once <?= $lowHeOrShe_15; ?> wakes up bring <?= $lowHimOrHer_15; ?> back to your shared mattress.</p><p> 
        Nights 5-6 Place <?= $lowHimOrHer_15; ?> in the crib, when <?= $lowHeOrShe_15; ?> wakes around midnight bring <?= $lowHimOrHer_15; ?> back to your shared mattress.</p><p>
        Nights 7-8 Let <?= $lowHimOrHer_15; ?> spend ¾ of the night in his crib and then at dawn bring <?= $lowHimOrHer_15; ?> into your shared mattress when <?= $lowHeOrShe_15; ?> wakes up if needed</p><p>
        Nights 9-10Leave <?= $lowHimOrHer_15; ?> in <?= $lowHisOrHer_15; ?> crib all night while you sleep in the mattress next to <?= $lowHimOrHer_15; ?> crib.</p><p>
        Night 11 Move the mattress out and leave <?= $lowHimOrHer_15; ?> in the crib. *** this is VERY gradual, and you can make it even more gradual by spending more than a couple nights in each step, but it does mean more time of sleep training.</p><p>
        A key to this method is to continue diminishing the time spent with you on the mattress. If you just give in every day at 9pm, you will not be sleep training him.</p><p>
         </p><p>
         <b><h3>Gradual Plan for Co-sleeping to Crib</h3></b></p><p>
         </p><p>
        Nights 1-3 Continue what you have been doing, but in <?= $lowHisOrHer_15; ?> room next to <?= $lowHisOrHer_15; ?> crib, so <?= $lowHeOrShe_15; ?> gets used to being near <?= $lowHisOrHer_15; ?> crib for bedtime.</p><p>
         Nights 4-5 Place <?= $lowHimOrHer_15; ?> in the crib for the night but keep your mattress near him so you’re right there and can tap him during the night if needed. Following nights- keep separating your mattress a little more from the crib each night until you can’t separate it more and you are out of the room.</p><p>
        *** this is gradual, and you can make it even more gradual by spending more than a couple nights in each step</p><p>
         </p><p>
        <b><h3>Straight Forward from Co-Sleeping to Crib</h3></b></p><p>
        </p><p>
        Night 1- Place <?= $lowHimOrHer_15; ?> in <?= $lowHisOrHer_15; ?> crib and you sleep in your own bed from the first night. *** this will be the most intense type of reaction from <?= $babyname_3; ?>, but the shorter amount of days spent crying.</p><p>
        Although I prefer gentler methods, I always have to remember that it means more time with crying, ‘harsher’ (for lack of a better word ) methods have quicker results. </p>
    <?php endif; ?>
<p></p>
    <?php if ( $cosleep_23 == 'Yes stay' ): ?>
        <p>
        Co-sleeping is entirely up to you and at your own risk. Please make sure you research the safest way to cosleep to make sure you are following appropriate guidelines. Co-Sleeping and Sleep training CAN happen but it does add days (or weeks) to the process. If you are choosing a sleep training method (from below) that includes time away from <?= $babyname_3; ?>, leaving <?= $lowHimOrHer_15; ?> by <?= $lowHimselfOrHerself_15; ?>  please make sure <?= $lowHeOrShe_15; ?> is in a safe environment, which will be at your own risk. </p>
    <?php endif; ?>
<p>
</p><p>
<b><h1>So what do I do at a night waking?</h1></b></p><p>
</p><p>
If <?= $babyname_3; ?> wakes up in the middle of the night immediately crying, it’s important to wait a few minutes before going to him. At 5-6 months you should wait at least 5-10 minutes before going to <?= $lowHimOrHer_15; ?> to try to help <?= $lowHimOrHer_15; ?> sleep. Some babies can cry hysterically for a bit as they change sleep cycles while in a semi-conscious state. Intervening may cause them to wake up completely. Giving your baby a few minutes allows them a chance to calm down, which may result in a sudden quieter, and whiny cry. That cry may then stop and baby may fall back to sleep on <?= $lowHimOrHer_15; ?> own. That’s why it's important to wait. Of course you know your baby best, and you may feel it’s time to go in right away. If <?= $lowHeOrShe_15; ?> wakes up just fussing and whining, also give <?= $lowHimOrHer_15; ?> time. Do not intervene until you see it is obvious <?= $lowHeOrShe_15; ?> will not be calming down on <?= $lowHimOrHer_15; ?> own.</p><p>
Repeat the method you have chosen (from below) for as long as you can. Do NOT make <?= $lowHimOrHer_15; ?> fall asleep before you put <?= $lowHimOrHer_15; ?> down. Tell yourself that sleep training at that point is for <?= $lowHimOrHer_15; ?> own good, and you will NOT resort to feeding <?= $lowHimOrHer_15; ?> to sleep. Whenever you do, you’re just getting in the way of <?= $lowHimOrHer_15; ?> learning to fall asleep unfortunately. Allowing <?= $lowHimOrHer_15; ?> to fall asleep without needing you is a great gift for your baby. (If you would like more help differentiating between comfort and hunger cues, please reach out to me.)</p><p>
I really advice against bringing <?= $lowHimOrHer_15; ?> into your bed in the middle of the night, especially as a last resource. It seems like a good solution in the wee hours when nothing else will help- but in reality it may be confusing <?= $lowHimOrHer_15; ?> more than helping <?= $lowHimOrHer_15; ?> in the long run. I suggest instead to try the sleep training method you choose until at least 4/5 am. Sometimes babies do need a different environment after 8/9 hours of sleeping on their backs/bellies in the crib, and so bringing them in for the last couple sleep cycles is not the worst idea if you know how to do this safely. But do this knowing that this is your plan, not as a last minute scramble.</p><p>
</p><p>
<h1>Sleep Training</h1></p><p>
When you’ve chosen a path you have to STICK to it. The worst thing you can do is try to sleep train if you are not 100% committed. So if for some reason you are not ready TODAY, next week, in a couple months, that’s ok! Wait a few more nights if you need. If you try something for a few days and then give up, you will be confusing <?= $babyname_3; ?> more, and it will result in a more frustrating experience. You have to be willing to give a method at least 2 weeks before giving up. Also make sure you explain the method to any adult that will be putting <?= $babyname_3; ?> down for any sleeps so that there is extreme consistency.</p><p>
 *If your chosen method makes you extremely uncomfortable you can of course choose a different method. I don’t want you to do something that makes you uneasy or anxious.</p><p>
 So when you are ready to start sleep training, choose a method from the ones below, think about what you can do and get started right away.</p><p> 
</p><p>
<h2>The Sleep Training Methods</h2></p><p>
I have outlined some methods of sleep training that are best suited for <?= $babyname_3; ?>’s age and temperament, and I’ve written them in order from least tears expected to more tear-filled methods. It’s important to note that the least tears also mean the most time spent crying. More tears mean quicker results.</p><p>
My husband and I opted for an extremely gentle method for naptraining our first baby and although it gave us the desired results, it honestly took a long time to get them. (Baby went from SCREAMING when placed in his crib to be able to move around for 10-15 minutes before settling down on his own.)</p><p>
Also important is something crucial: you are tired. Some of the more gentle methods can be draining on an already exhausted parent. They may, however, be the ones you like best, so give them all a good read and see which one you can do. All of these are a million times easier with someone by your side. If possible to have a friend or your another family member help you out, that would be ideal. </p><p>
</p>
    <?php if ( $crying_29 == 'Some crying' ): ?>
    <p>
    I’m glad you are ok with some crying- teaching babies anything will undoubtedly take some crying, simply because it’s the only way they can express themselves. Sometimes babies learn sleep without absolutely any crying, but this is very rare. It’s going to be important to really get to know baby to know when they’re crying out of frustration, when they're simply crying because they're tired, and when they’re crying because they really need you. Remember that teaching them independent sleep is allowing them to sleep better and also giving them better rested parents! I added a super gentle method for you to read because maybe you want to try it out or see what a super “non-crying” method looks like, but for you I truly recommend you try either the <If Age under 6> “Pat and Shush” or “Pick up Put Down” </If Age Under 6> < If Age Over 6 months> “Chair Method” or the “Check-In Method.” </If Age Over 6 Months> </If Q29 Input: don’t mind hearing some crying></p>
    <?php endif; ?>

    <?php if ( $crying_29 == 'Loud crying' ): ?>
        <p>
        I’m glad you are ok with some crying- teaching babies anything will undoubtedly take some crying, simply because it’s the only way they can express themselves. Sometimes babies learn sleep without absolutely any crying, but this is very rare. It’s going to be important to really get to know baby to know when they’re crying out of frustration, when they are simply crying because they are tired, and when they’re crying because they need you. Remember that teaching them independent sleep is allowing them to sleep better and also giving them better rested parents!</p><p>
         I added some more gentle methods for you to read because maybe you want to try those out or just see what they look like, but for you I truly recommend you try the “Check-In Method.” </p>
    <?php endif; ?>

    <?php if ( $crying_29 == 'No crying' ): ?>
    <p>
    Sometimes babies learn sleep without absolutely any crying, but this is very rare. It’s going to be important to really get to know baby to know when they’re crying out of frustration, when they are tired from being tired, and when they’re crying because they need you. Remember that teaching them independent sleep is allowing them to sleep better and also giving them better rested parents! I added a super gentle method for you to read and try out, but please be aware that this method can take up to weeks or even months. I also added other less involved methods that are a little quicker but do include a bit more tears. If you would like results in the next few days then I suggest using those.</p><p>
     <If Age under 6> “Pat and Shush” or “Pick up Put Down” </If Age Under 6> < If Age Over 6 months> “Chair Method” or the even the “Check-In Method” but that one is the least involved </If Age Over 6 Months> </p><p>
    <?php endif; ?>

</p><p>
<h2>METHODS </h2> </p><p>
<h3>Gentle Prop Elimination</h3></p><p>
</p><p>
At bedtime or when <?= $lowHeOrShe_15; ?> wakes up (and after you’ve waited 5-10 minutes)  

    <?php $i = 0;
            $ilen = count( $sleepcrutches_25 );
            foreach( $sleepcrutches_25 as $key => $value )
            {
                if( ++$i == $ilen ) {echo "$value";
                break;
                }
               echo "$value/";
            }
    ?>

 is okay for a little bit. The first time let <?= $lowHimOrHer_15; ?> be very drowsy but not entirely asleep. The next night let <?= $lowHimOrHer_15; ?> be a little less drowsy, make sure <?= $lowHeOrShe_15; ?> wakes up, and then place <?= $lowHimOrHer_15; ?> down in <?= $lowHimOrHer_15; ?> crib. This gentle prop elimination can take many WEEKS to months to work, but it is the least tear-filled method. I only recommend this method when you cannot hear ANY crying at all, but I have to warn you that it is very slow and will not work with all babies. I wanted to write this one in so you know it exists, but to really steer you away from it unless all the other ones do not sound feasible. </p><p>
</p><p>
<b><h3>The Chair Method</h3></b></p><p>
 </p><p>
For this method you will have a chair and place it right next to the crib. You can pat <?= $lowHimOrHer_15; ?> or shush <?= $lowHimOrHer_15; ?> whenever <?= $lowHeOrShe_15; ?> stirs but the idea is not to pick <?= $lowHimOrHer_15; ?> up and rock him. Also try not to pat <?= $lowHimOrHer_15; ?> TO sleep.</p><p>
 Remove your hand from <?= $lowHimOrHer_15; ?> body whenever <?= $lowHeOrShe_15; ?> is calm. If <?= $lowHeOrShe_15; ?> stirs again you can help <?= $lowHimOrHer_15; ?> again and <?= $lowHeOrShe_15; ?> will know you are right next to him. Do this until <?= $lowHeOrShe_15; ?> has fallen asleep.</p><p>
The next night move your chair a foot away from the crib/bed. Your response time will be just slightly delayed each time. Repeat the same process until <?= $lowHeOrShe_15; ?> is asleep.</p><p>
 Every night move your chair farther and farther away from <?= $lowHimOrHer_15; ?> crib. Remember to only calm <?= $lowHimOrHer_15; ?> down when you know <?= $lowHeOrShe_15; ?> needs your comfort, let <?= $lowHimOrHer_15; ?> fuss a little bit as he’s falling asleep.</p><p>
After some nights the chair will be in the doorway and then outside of the room.
The chair method for middle of the night:</p><p>
 1. After 5-15 minutes of hearing <?= $lowHimOrHer_15; ?> protest, go in to calm <?= $babyname_3; ?> down. Make sure <?= $lowHeOrShe_15; ?> does not fall asleep on you.</p><p>
 2. Once <?= $babyname_3; ?> is lying in the crib calmly and drowsy, but not yet asleep, sit on the chair right next to the crib. Stay there until <?= $lowHeOrShe_15; ?> falls asleep.</p><p>
 3. If <?= $lowHeOrShe_15; ?> cries a lot you can calmly pat <?= $lowHimOrHer_15; ?> (unless it aggravates <?= $lowHimOrHer_15; ?>, in this case DON’T touch him). But this way <?= $lowHeOrShe_15; ?> can see you and the transition is less painful.</p><p>
4. The next night set the chair up a bit farther away from the crib. If <?= $lowHeOrShe_15; ?> fusses a lot you can still calmly pat <?= $lowHimOrHer_15; ?> and then return to your chair.</p><p>
5. Move your chair back farther away from <?= $lowHimOrHer_15; ?> each night until you are out of the room.</p><p>
</p><p>
A key to this method is to let <?= $lowHimOrHer_15; ?> fuss a little before responding, and to never give in to rocking or feeding <?= $lowHimOrHer_15; ?> to sleep. Also, it’s important to differentiate protesting and full scale inconsolable crying. Be sure you only respond when it gets to crying. The important thing in the Chair Method is to distance yourself a little more each time, to wean <?= $lowHimOrHer_15; ?> away from needing you to fall asleep. Now a warning: this method could prolong the sleep training a LONG time, but I do recommend it as a top method for those wanting to avoid tears.</p><p>
</p><p>
<Only include if Age under 7 months> <h3>The Pat and Shush Method:</h3></p><p>
When you place <?= $babyname_3; ?> in <?= $lowHimOrHer_15; ?> crib start to make the ‘shh’ sound and pat <?= $lowHimOrHer_15; ?> firmly in the center of the back (or stomach or side). Do this shush pat to calm <?= $babyname_3; ?> down and then keep going for another 7-10 minutes. After that, slow down the patting and eventually stop the ‘shh.’ Walk away from baby and stay in a place <?= $lowHeOrShe_15; ?> cannot see you. Watch <?= $lowHimOrHer_15; ?> to see if <?= $lowHeOrShe_15; ?> is able to fall asleep on <?= $lowHimOrHer_15; ?> own. If <?= $lowHeOrShe_15; ?> starts crying again, try the shush pat again.</p><p>
Repeat until baby is asleep.</p><p>
 </p><p>
This may take a long time at first but will decrease if done with consistency and always making sure baby doesn’t fall asleep TO the prop of the pat and shush. After a few nights cut those 7-10 minutes shorter and see if <?= $lowHeOrShe_15; ?> can calm himself down with less patting and shushing until all you need to do is place <?= $lowHimOrHer_15; ?> in hiscrib and walk away. This can take weeks but it includes very few tears on <?= $lowHimOrHer_15; ?> own. </p><p>
</p><p>
A warning about this method: not all babies like to be pat and shushed, even though the majority does. If you have tried patting for 5 minutes and baby will not calm down, this will not be your method.</p><p>
</p><p>
<h3>Pick up- Put down**</h3></p><p>
Place <?= $babyname_3; ?> down in <?= $lowHimOrHer_15; ?> crib and exit the room. If <?= $lowHeOrShe_15; ?> cries let <?= $lowHimOrHer_15; ?> fuss around for 5 minutes before you go into the room.</p><p>
 When you go into the room assess the situation. If <?= $lowHeOrShe_15; ?> is crying hysterically, pick <?= $lowHimOrHer_15; ?> up and calm <?= $lowHimOrHer_15; ?> down but be careful not to let <?= $lowHimOrHer_15; ?> fall asleep on you. When you are calming <?= $lowHimOrHer_15; ?> down be careful not to interact too much with him. Don’t talk to <?= $lowHimOrHer_15; ?> or rock <?= $lowHimOrHer_15; ?> too much. Place <?= $lowHimOrHer_15; ?> back in <?= $lowHimOrHer_15; ?> crib when <?= $lowHeOrShe_15; ?> is calm and walk out of the room again.</p><p>
 If <?= $lowHeOrShe_15; ?> is crying but not hysterically you can simply Pat and Shush <?= $lowHimOrHer_15; ?> until <?= $lowHeOrShe_15; ?> is more calm and then walk out of the room. Be careful not to put <?= $lowHimOrHer_15; ?> to sleep entirely .</p><p>
 </p><p>
For middle of the night:</p><p>
1.   After the 5-15 minute wait go in to calm <?= $babyname_3; ?> down if <?= $lowHeOrShe_15; ?> is screaming or crying very loudly.</p><p>
2.   Once you lay <?= $babyname_3; ?> down leave the room to let <?= $lowHimOrHer_15; ?> settle on own. If you hear <?= $lowHimOrHer_15; ?> crying (not just protesting) you can come back in and offer some comforting words (or a song), place your hand on <?= $lowHimOrHer_15; ?> back, and even pat <?= $lowHimOrHer_15; ?> while you shush in <?= $lowHimOrHer_15; ?> ear.</p><p>
3.   If this does not comfort <?= $lowHimOrHer_15; ?> after a short period of time, then pick <?= $lowHimOrHer_15; ?> up. As soon as <?= $lowHeOrShe_15; ?> is not scream crying anymore, lay <?= $lowHimOrHer_15; ?> back down with a simple phrase, such as “time to go to sleep,etc.”</p><p>
4.   If <?= $lowHeOrShe_15; ?> starts crying again (most likely) then pick <?= $lowHimOrHer_15; ?> back up and repeat the process as needed.</p><p>
</p><p>
When you pick <?= $lowHimOrHer_15; ?> up, don’t do anything that may qualify as fun and try not to sway too much so <?= $lowHeOrShe_15; ?> does not fall asleep in your arms. Just hold <?= $lowHimOrHer_15; ?> and calm <?= $lowHimOrHer_15; ?> down until <?= $lowHeOrShe_15; ?> is calm, and place himback down. At this age you don’t want to be holding <?= $lowHimOrHer_15; ?> for longer than 2-3 minutes, because you can become a different kind of sleep prop. If you’ve held <?= $lowHimOrHer_15; ?> for this long and <?= $lowHeOrShe_15; ?> is still not calm, lay <?= $lowHimOrHer_15; ?> down completely and then pick <?= $lowHimOrHer_15; ?> back up to repeat the process.</p><p>
</p><p>
This method can last a long time the first few times you use it, but it will get better the more consistent you are. I suggest you have someone help you with it because it will not be easy to hear <?= $lowHimOrHer_15; ?> crying out by yourself. (From experience it is also not easy on your back) ** some babies get more aggravated when picked up.</p><p>
 If <?= $babyname_3; ?> arches <?= $lowHimOrHer_15; ?> back and wails as soon as you pick <?= $lowHimOrHer_15; ?> up, this will not be the method for you. This method has an expectancy of 2-4 weeks for results at nighttime. Though if <?= $babyname_3; ?> is good at sleeping, it could come earlier.
</p><p>
</Only include if Age under 7 months> 
</p><p>
<h3>The Check In Method</h3></p><p>
 If you are comfortable with this method, I suggest using this one as it is the quickest for results. It does have a prerequisite that is crucial for success, however, a WELL rested baby and so I suggest fixing schedule FIRST and then trying a method like this. <If Q27 Input: YES> I know you tried the Ferber method before and it didn’t work out for you, but perhaps you tried it without making sure <?= $lowHeOrShe_15; ?> was perfectly tired, that <?= $lowHimOrHer_15; ?> schedule was perfect, and that the environment was sleep conducive. Creating a perfect sleep environment will be a huge change if you choose to try this version of the Ferber Method </If Q27 Input: YES></p><p>
1.   Leave <?= $lowHimOrHer_15; ?> lying on the crib when <?= $lowHeOrShe_15; ?> is calm or just crying a little bit and leave the room.</p><p>
2.   Come back and check in on <?= $lowHimOrHer_15; ?> after a set period of time. It really doesn’t matter how long the time is as long as it is consistent. Don’t go into the room to check in on <?= $lowHimOrHer_15; ?> AS SOON as you hear <?= $lowHimOrHer_15; ?> crying. (Ie if your set time is 3 minutes, then only go every 3 minutes. If your set time is 15, only go in at 15. I recommend you use a timer, because when you hear a baby crying, 2 minutes can feel like 30). If <?= $lowHeOrShe_15; ?> stops crying, reset the timer. At this age I recommend at LEAST 5 minutes.</p><p>
3.   When you go in the room, only say a set phrase and say it calmly. It could be, “time to sleep/ sleepy time <?= $babyname_3; ?> / go night night” Be careful not to engage in a “conversation” with <?= $lowHimOrHer_15; ?> because this will only stimulate <?= $lowHimOrHer_15; ?> more.</p><p>
4.   Set <?= $lowHimOrHer_15; ?> back in the middle of the crib, make sure <?= $lowHeOrShe_15; ?> hasn’t wet himself, and leave the room again.</p><p>
5.   Come back in a previously SET amount of time. Don’t let <?= $lowHimOrHer_15; ?> think that if <?= $lowHeOrShe_15; ?> just cries loud enough you will come give <?= $lowHimOrHer_15; ?> what <?= $lowHeOrShe_15; ?> wants.</p><p>
 
If you do not give in <?= $lowHeOrShe_15; ?> should fall asleep within 60-90 minutes of the Check-in Method (or much less in many cases) (if <?= $lowHeOrShe_15; ?> is not overtired from the day). That sounds like a lot, but think of it in their point of view. Would you cry for an hour or two the next night if it didn’t get you what you wanted?</p><p>
 • DO NOT give in to your old sleep crutches [
 
        <?php $i = 0;
                $ilen = count( $sleepcrutches_25 );
                foreach( $sleepcrutches_25 as $key => $value )
                {
                    if( ++$i == $ilen ) {echo "$value";
                    break;
                    }
                   echo "$value/";
                }
        ?>
 
]. This is exactly the habit you’re trying to break to teach <?= $lowHimOrHer_15; ?> how to fall asleep.</p><p>
 
One or two hours that will feel like an ETERNITY to you. As a mom we are wired to feel cries even MORE, so if you can, have someone else do this process. If <?= $lowHeOrShe_15; ?> has not fallen asleep and <?= $lowHeOrShe_15; ?> has gotten into a stage you would call hysterical, or one that really worries you ie throwing up, get any other prop and use it to help <?= $lowHimOrHer_15; ?> fall asleep: rock, sway, hug, pat, etc. At this point we want to make sure you just DON’T feed <?= $lowHimOrHer_15; ?> to sleep. <?= $lowHeOrShe_15; ?> will get the message: I am helping you to sleep but you Will not be falling asleep to milk. Again, I suggest someone ELSE do the rocking, swaying, hugging, because <?= $lowHeOrShe_15; ?> will be so close to what <?= $lowHeOrShe_15; ?> wants. Try it again the next night, and the next, and then with more space between each check in. This method SHOULD work if you are extremely consistent with it.</p><p>
 </p><p>
<h2>Stirring an hour or so after bedtime</h2></p><p>
My baby both does this (I’ve noticed as I watch <?= $lowHimOrHer_15; ?> on our webcam) around 45 minutes after he falls asleep. He starts groaning and moving around. This is <?= $lowHimOrHer_15; ?> waking up from <?= $lowHimOrHer_15; ?> first sleep cycle, but still tired enough from the day not to full out cry for me. If you notice this or hear <?= $lowHimOrHer_15; ?> moving around, don’t go in to help <?= $lowHimOrHer_15; ?>, let <?= $lowHimOrHer_15; ?> figure out the entrance into <?= $lowHimOrHer_15; ?> next sleep cycle. If <?= $lowHeOrShe_15; ?> does not go back to sleep and starts screaming then use whatever method you have chosen for bedtime, and follow it until <?= $lowHeOrShe_15; ?> has fallen back asleep. Remember, consistency is KEY.</p><p>
</p><p>
<h2>Bedtime or Naptime?</h2></p><p>
I suggest you do the “first sleep” of sleep training at bedtime. The push to sleep is stronger, <?= $lowHeOrShe_15; ?> will be more tired. I also suggest you sleep train at bedtime first, and when you have achieved that goal of independent sleep, THEN start to naptrain. Bedtime sleep is much easier than naptime. This means, again, continue to nurse <?= $lowHimOrHer_15; ?> and hold <?= $lowHimOrHer_15; ?> for naps if possible - anything to get <?= $lowHimOrHer_15; ?> to bedtime not overtired. Once <?= $lowHeOrShe_15; ?> is sleeping through the night or only waking up once or twice, then start working on naps. The key for naps is to respect sleep cues and waketimes as much as possible.</p><p>
Another big thing is to try to have longer naps while sleep training for the night. I mentioned this above, but the way you do this is this: be very watchful of <?= $lowHimOrHer_15; ?> during <?= $lowHimOrHer_15; ?> nap. When <?= $lowHeOrShe_15; ?> starts to move (after 30-45 minutes usually) tap and shush <?= $lowHimOrHer_15; ?> or even feed <?= $lowHimOrHer_15; ?>, to encourage <?= $lowHimOrHer_15; ?> to move into a second nap cycle. <?= $lowHeOrShe_15; ?> may need to sleep on you for this second cycle, that’s fine. We’re just going to work on longer naps at first, we’ll put the method to work when we’re ready to nap train. Most babies stop taking long naps around 3-4 months and then consolidate them around 7 or so months, so don’t let it stress you out! </p><p>
<p>If you're wondering why I've talked about doing bedtime first three times now it's because after three years of doing sleep plans I almost ALWAYS get the question, "should I do naps or beditme first?" even after sending the plan, so I'm just covering my bases ;) </p><p>
<h1>Future Possible Issues</h1></p><p>
</p><p>
<h2>Teething</h2><p>
Teething has a horrible rep amongst moms. We love to blame teething on sleep. In reality teething should only mess with 1 or 2 nights of sleep (per tooth at a time). If baby is getting up during the night for longer than that it means <?= $lowHeOrShe_15; ?> has developed a bad habit. Whenever <?= $babyname_3; ?> wakes up with pain from teething help <?= $lowHimOrHer_15; ?>, soothe<?= $lowHimOrHer_15; ?>, give <?= $lowHimOrHer_15; ?> medicine if that is what you are ok with, but don’t help <?= $lowHimOrHer_15; ?> fall asleep or feed <?= $lowHimOrHer_15; ?> when <?= $lowHeOrShe_15; ?> is not hungry. That way you can be sure that if <?= $lowHeOrShe_15; ?> cries it’s because of pain (and can therefore help him) and not just wanting to appease a bad habit.</p><p>
</p>
    <?php if ( $age_5_months <= 5 ): ?>
        <p><h2>Usually around 4/5 months</h2></p>
        <p>Rolling</p><p>
        When baby starts to roll it can really affect sleep. Try to let <?= $lowHimOrHer_15; ?> figure it out for <?= $lowHimselfOrHerself_15; ?>  as much as possible so that they are able to go back to sleep once they find a comfortable position.</p>
    <?php endif; ?>
    
    <?php if ( $age_5_months <= 7 ): ?>
        <p><h2>Usually around 6/7 months</h2></p>
        <p><h2>Sitting</h2></p>
        <p>Sometimes when baby starts sitting down it will mess with sleep because they want to keep sitting down all night! Practice sitting down and let them fall! I don’t mean to let them get hurt but set up pillows and or cushions around them as they’re learning to sit by themselves so that they can learn to lay back down on their own during the day. If baby keeps sitting up at night, lay them back down once but then try to let <?= $lowHimOrHer_15; ?> figure it out on <?= $lowHimOrHer_15; ?> own as much as possible.</p>
    <?php endif; ?>
    
<p><h2>Usually around 7/8 months</h2></p><p>
 </p><p>
Standing</p><p>
 Laying down baby each time they stand can turn into a fun game for baby. Let baby get back down on <?= $lowHimOrHer_15; ?> own if it has turned into a game. Also what you can do to prevent is to teach <?= $lowHimOrHer_15; ?> before <?= $lowHeOrShe_15; ?> needs it. Stand <?= $lowHimOrHer_15; ?> up while <?= $lowHeOrShe_15; ?> holds on to you and teach <?= $lowHimOrHer_15; ?> to bend <?= $lowHimOrHer_15; ?> legs to sit back down for a long time. This way when <?= $lowHeOrShe_15; ?> is standing on <?= $lowHimOrHer_15; ?> own, he’ll know how to get back down!</p><p>
 </p><p>
<h2>Usually 11-12 months</h2></p><p>
Language Development</p><p>
Babies will be thinking of words they have learned and may start to practice them in their heads, instead of sleeping. If baby is talking happily instead of sleeping, that’s perfectly fine, even if it takes them longer to fall asleep.</p><p>
</p><p> 
<h2>12 month regression</h2></p><p>
At this age babies will be going through tremendous physical development, learning to walk and even climb. This will most likely carry into sleep. Just like with any other regression, offer extra attention before bedtime if it works, but don’t start any new habits, and stick to the same plan of having baby sleep on <?= $lowHimOrHer_15; ?> own.</p><p>
 </p><p>
Testing Parents</p><p>
At this age, <?= $babyname_3; ?> will enjoy testing you. Now that <?= $lowHeOrShe_15; ?> is discovering that what <?= $lowHeOrShe_15; ?> does has a direct effect on your reactions, <?= $lowHeOrShe_15; ?> may start to refuse naps and bedtime, just to see what you will do. Even if you always had a very “easy baby” up until this point, <?= $lowHeOrShe_15; ?> would now start to fight sleep at all times. This is usually behavioral and it should be addressed by not giving into the behavior, and instead sticking to the rules of good sleep habits.</p><p>
</p>

    <?php if ( $joke_30 == 'Yes' ): ?>
    <p> If baby won’t nap, does that mean they’re resisting arrest?</p>
    <p></p>
    <?php endif; ?>

    <?php if ( $otherkids_17 == 'Yes' ): ?>
        <p><h1>Other Kids at Home</h1></p><p> 
        This sleep plan is fairly simple to follow when you only have one baby. I am a mom of two currently and I recognize that this isn’t all possible when you have more than one little tiny sleeping creature to take care of! So first of all, be easy on yourself! Remember that a sleep plan is important and it will help baby sleep but if you cannot apply it 100% because of other responsibilities, that is ok! Aim for the best that you can do!</p><p>
        </p><p>
        But here are some words to help you out if you are very anxious about sleep training while dealing with older siblings:</p><p>
        </p><p>
        Sometimes older siblings DO wake up because baby is crying. In this case reassure your older kiddos that baby is OK. Many times older siblings are worried that their new baby is crying, tell them that mommy/daddy will be there for baby, they need not worry!</p><p>
        Also make sure everyone has their own white noise machine, in their room, to block out crying, especially if you’ll be using a method that includes a bit louder crying. </p><p>
        </p><p>
        Remember this is just a phase. Soon baby will be older and will need a lot less attention, and the sooner baby is sleeping better, the sooner this will be! Focus on what you can, ask for help from anyone you can, and give yourself a break when needed!</p><p>
        </p><p>
        When working on naps your older kiddos may need more attention, don’t feel guilty if you need to put on a little bit of screen time for them, or find quiet activities that they love and can only do when it’s time for naptime. Depending on their personalities you could also give them “jobs” to do when it’s time for naptime. Perhaps their “job” is to make sure no one goes into baby’s room, or to make sure all the windows are closed... etc, be creative! </p><p></p>
    <?php endif; ?>

    <?php if ( $scheduleconflicts_18 == 'Yes' ): ?>
    <p>
    <b>Other Activities getting in the way</b></p><p>
    Since you have some activities that could get in the way of a good schedule my first advice is to: breathe. Remember this is a stage in life, and one or two missed naps a week won’t mess with a baby’s entire sleep hygiene. Having said that, sleep does need to be a priority! Sleep is crucial to a baby’s development (and daily mood) so keep that in mind. Perhaps you may have to find some help watching baby when you have conflicting activities, or you’ll have to get creative for sleeping on the go- find something that works for your family and that you find time to reassess your choices depending on how baby reacts! </p><p></p>
    <?php endif; ?>

<p></p><p>
 </p><p>
<h1>Going forward</h1></p><p><b>
I hope this information helps you and your family sleep better and easier! Remember, every strong step you take to make <?= $lowHimOrHer_15; ?> sleep better will only make <?= $lowHimOrHer_15; ?> stronger!</b></p><p><b>
The focus of this plan is to teach <?= $babyname_3; ?> independent sleep. I have outlined different options of handling <?= $lowHimOrHer_15; ?> crying. If you have questions please consider purchasing a coaching package.</b></p><p> <b></b></p><p><b>
With care,</b></p><p><b>
Andrea De La Torre</b></p><p>
</p>
</p><p>
</p><p>
</p><p>
<pagebreak>
</pagebreak>
<p><b>
Medical Disclaimer</b></p><p><b>
*Disclaimer The information/advice/ provided during this consultation is not medical advice. Reliance on the advice is solely at your own risk. The advice is for informational purposes only and is intended for use with common sleep issues that are unrelated to medical conditions. The information provided is not intended nor is implied to be a substitute for professional medical advice. Always seek the advice of your physician with any questions you may have regarding a medical condition or the health and welfare of your baby, and before following the advice or using the techniques offered in this consultation. Also, It is always best to follow sleep recommendations according to the SIDS campaign. In no event will Andrea De La Torre be liable to you for any claims, losses, injury or damages as a result of reliance on the information provided.</b></p><p><b>
Copyright © Andrea De La Torre 2020, All rights reserved.</b></p><p>
 </p><p><b>
No part of this publication may be reproduced, republished, or transmitted in any form or by any means for commercial use, mechanical or electronic, including photocopying and recording, or by any information storage and retrieval system, without permission in writing from the publisher. Any passing of any information to anyone is strictly forbidden and subject to International copyright laws.</b></p><p>
</p>
<pagebreak></pagebreak>
<h1>Your responses to the Sleep Survey</h1>
<?php
    
    /*
     * Load our core-specific styles from our PDF settings which will be passed to the PDF template $config array
     */
    $show_form_title      = ( ! empty( $settings['show_form_title'] ) && $settings['show_form_title'] == 'Yes' )            ? true : false;
    $show_page_names      = ( ! empty( $settings['show_page_names'] ) && $settings['show_page_names'] == 'Yes' )            ? true : false;
    $show_html            = ( ! empty( $settings['show_html'] ) && $settings['show_html'] == 'Yes' )                        ? true : false;
    $show_section_content = ( ! empty( $settings['show_section_content'] ) && $settings['show_section_content'] == 'Yes' )  ? true : false;
    $enable_conditional   = ( ! empty( $settings['enable_conditional'] ) && $settings['enable_conditional'] == 'Yes' )      ? true : false;
    $show_empty           = ( ! empty( $settings['show_empty'] ) && $settings['show_empty'] == 'Yes' )                      ? true : false;
    
    /**
     * Set up our configuration array to control what is and is not shown in the generated PDF
     *
     * @var array
     */
    $html_config = array(
        'settings' => $settings,
        'meta'     => array(
            'echo'                     => true, /* whether to output the HTML or return it */
            'exclude'                  => true, /* whether we should exclude fields with a CSS value of 'exclude'. Default to true */
            'empty'                    => $show_empty, /* whether to show empty fields or not. Default is false */
            'conditional'              => $enable_conditional, /* whether we should skip fields hidden with conditional logic. Default to true. */
            'show_title'               => $show_form_title, /* whether we should show the form title. Default to true */
            'section_content'          => $show_section_content, /* whether we should include a section breaks content. Default to false */
            'page_names'               => $show_page_names, /* whether we should show the form's page names. Default to false */
            'html_field'               => $show_html, /* whether we should show the form's html fields. Default to false */
            'individual_products'      => false, /* Whether to show individual fields in the entry. Default to false - they are grouped together at the end of the form */
            'enable_css_ready_classes' => true, /* Whether to enable or disable Gravity Forms CSS Ready Class support in your PDF */
        ),
    );
    
    /*
     * Generate our HTML markup
     *
     * You can access Gravity PDFs common functions and classes through our API wrapper class "GPDFAPI"
     */
    $pdf = GPDFAPI::get_pdf_class();
    $pdf->process_html_structure( $entry, GPDFAPI::get_pdf_class( 'model' ), $html_config );
?>

<?php if ( $show_meta_data === 'Yes' ): ?>
	<p>
		<strong>User IP:</strong> <?php echo $form_data['misc']['ip']; ?><br>
		<strong>Submission Timestamp:</strong> <?php echo $form_data['misc']['date_time']; ?><br>
		<strong>User Agent:</strong> <?php echo $form_data['misc']['user_agent']; ?><br>
		<strong>Source URL:</strong> <?php echo $form_data['misc']['source_url']; ?>
	</p>
<?php endif; ?>